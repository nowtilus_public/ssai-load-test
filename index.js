require('dotenv-defaults').config()
const playerNumber = Number(process.env.CONCURRENT_PLAYERS || 2)
const playerStartSleepMs = Number(process.env.PLAYER_START_SLEEP_MS || 600)
console.log('starting ' + playerNumber + ' players')

// Allow as much event listeners as needed
require("events").EventEmitter.defaultMaxListeners = playerNumber + 10
const { promisify } = require("util")
const sleep = promisify(setTimeout)
const Player = require('./player')

// metrics:
const promClient = require('prom-client')
const metrics = {
  vplayer_requests_total: new promClient.Counter({
    name: 'vplayer_requests_total',
    help: 'vplayer_requests_total',
    labelNames: ['playerType', 'testName']
  }),
  vplayer_success_responses_total: new promClient.Counter({
    name: 'vplayer_success_responses_total',
    help: 'vplayer_success_responses_total',
    labelNames: ['playerType', 'testName']
  }),
  vplayer_error_responses_total: new promClient.Counter({
    name: 'vplayer_error_responses_total',
    help: 'vplayer_error_responses_total',
    labelNames: ['playerType', 'testName', 'httpCode1', 'httpCode2']
  }),
  vplayer_success_responses_timing: new promClient.Histogram({
    name: 'vplayer_success_responses_timing',
    help: 'vplayer_success_responses_timing',
    labelNames: ['playerType', 'testName', 'httpCode1', 'httpCode2'],
    units: 'seconds',
    buckets: [0, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 50]
  })
}


// metrics server:
const fastify = require('fastify')({ logger: true })
fastify.get('/metrics', async (request, reply) => {
  return promClient.register.metrics()
})
const start = async () => {
  try {
    const port = +process.env.METRICS_PORT || 3333
    console.log(`will try to set up metrics endpoint at port ${port}`)
    await fastify.listen(port, '0.0.0.0')
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()

async function spinUpPlayers() {
  if (!process.env.URL || !process.env.PLAYER_TYPE)
    throw new Error(
      "You have to provide the environment variables URL and PLAYER_TYPE"
    )

  options = {
    player: process.env.PLAYER_TYPE,
    url: process.env.URL,
    metrics,
    testName: process.env.TEST_NAME
  }

  let players = []
  for (let i = 0; i < playerNumber; i++) {
    try {
      options.postMessage = async ({ message, i }) => {
        if (process.env.LOG_LEVEL === "info") {
          console.log(i + " " + JSON.stringify(message))
        }
      }

      options.exit = (i) => {
        delete players.find(p => p.options.i === i)
      }

      console.log(
        "[" + new Date().toISOString() + "] starting player instance " + i
      )

      console.log(`setting up player: ${options.player}`)

      const player = Player(options, (message) => {
        if (options.postMessage) options.postMessage({ message: message, i })
        else console.log(message)
      })
      players.push(player)
      player.start(options)
      await sleep(playerStartSleepMs)
    } catch (err) {
      console.error(i, err)
      delete players.find(p => p.options.i === i)
    }
  }
}

spinUpPlayers()
