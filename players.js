

const { promisify } = require("util")
const sleep = promisify(setTimeout)
const fetch = require("node-fetch")
const m3u8Parser = require("m3u8-parser")
const uuid = require("uuid/v4")
const vast = require("./vast")

const hlsParser = body => {
  const parser = new m3u8Parser.Parser()
  parser.push(body)
  parser.end()
  return parser.manifest
}
// const HLS = require("hls-parser");
const dashParser = require("mpd-parser");

/*
function buildVodRequest(type) {
    return {
        url:
            // CMS Mock
            "https://vod-cms-mock.azurewebsites.net/api/vod-cms-mock?type=" + type,
        // HLS
        //   "https://vodcms.blob.core.windows.net/mrss/serverside_mrss_hls_preroll.mrss",
        //   "https://vodcms.blob.core.windows.net/mrss/serverside_mrss_hls_preroll_midroll.mrss",
        //  DASH
        // https://vodcms.blob.core.windows.net/mrss/serverside_mrss_dash_preroll_midroll.mrss
        context: "web",
        ifa: "ifa",
        ad_channel_id: "&265585",
        ifa: "ifa",
        player_width: 1280,
        player_height: 720,
        deliverytypes: [type]
    };
}
*/
const sessionId = uuid()
const headers = {
  Connection: "Keep-Alive",
  "User-Agent":
    "User-Agent: AppleCoreMedia/1.0.0.19C57 (Macintosh; U; Intel Mac OS X 10_15_2; de_de)",
  "X-Playback-Session-Id": sessionId
}

function errorHandler(e, options = {}, errorsInRow, type = 'hls-live') {
  options.metrics.vplayer_error_responses_total.inc({ playerType: type, testName: options.testName })
  console.error("Error fetching playlist: ", e)
  if (errorsInRow > 4) log("More than 5 errors in a row. Last Error: " + e.message)
}

const players = (log) => {
  return {
    /*
      "hls-vod": {
          async init(url, type) {
              const body = buildVodRequest(type.split("-")[0]);
              headers["api-key"] = process.env.API_KEY;
              headers["content-type"] = "application/json";
              const response = await fetch(url, {
                  method: "post",
                  headers,
                  body: JSON.stringify(body)
              });
              const parsedBody = await response.json();
              const manifestResponse = await fetch(parsedBody.hls.url);
              const manifestText = await manifestResponse.text();
              const parsedManifest = await HLS.parse(manifestText);
              return parsedManifest;
          },
          errorsInRow: 0,
              async play(manifest, manifestUrl) {
              try {
                  const variant = manifest.variants[manifest.variants.length - 1].uri;
                  const subResponse = await fetch(variant, { headers });
                  const subBody = await subResponse.text();
                  const subManifest = HLS.parse(subBody);
                  const lastSegment = subManifest.segments.length - 1;
                  log(
                      "Watching current segment: " +
                      subManifest.segments[lastSegment].title +
                      " - " +
                      subManifest.segments[lastSegment].uri
                  );
                  this.errorsInRow = 0;
              } catch (e) {
                  log("Error fetching segment" + subManifest.segments[lastSegment].title);
                  throw new Error(e.message);
              }

              await sleep(60 * 1000); // request again every minute
              const newManifest = await this.init(manifestUrl, "hls");
              this.play(newManifest, manifestUrl);
          }
      },
      "dash-vod": {
          async init(url, type) {
              const body = buildVodRequest(type.split("-")[0]);
              headers["api-key"] = process.env.API_KEY;
              headers["content-type"] = "application/json";
              const response = await fetch(url, {
                  method: "post",
                  headers,
                  body: JSON.stringify(body)
              });
              const parsedBody = await response.json();
              const manifestResponse = await fetch(parsedBody.dash.url);
              const manifestText = await manifestResponse.text();
              const parsedManifest = dashParser.parse(manifestText, url);
              return parsedManifest;
          },
          errorsInRow: 0,
              async play(manifest, manifestUrl) {
              log(
                  `Playing ${manifest.playlists.length} playlists for ${Math.round(
                      manifest.duration / 0.6
                  ) / 100} Minutes`
              );
              await sleep(60 * 1000); // request again every minute
              const newManifest = await this.init(manifestUrl, "dash-vod");
              this.play(newManifest, manifestUrl);
          }
      },*/
    "hls-live": {
      async init(url) {
        headers.Referer = url
        try {
          const response = await fetch(url, { headers })
          if (response.status > 399) throw new Error('Request not successful: ' + response.status)
          const body = await response.text()
          const manifest = hlsParser(body)
          return manifest
        } catch (e) {
          log('Cannot fetch manifest: ' + e.message)
          throw e
        }
      },
      errorsInRow: 0,
      async play(manifest, manifestUrl, options, insideAdBreak = false) {
        let sleepDuration = 5000
        let playlist
        try {
          const baseUrl = manifestUrl.split("master.m3u8")[0]
          const variant = manifest.playlists[manifest.playlists.length - 1].uri
          playlist = baseUrl + variant

          options.metrics.vplayer_requests_total.inc({ playerType: 'hls-live', testName: options.testName })
          const timeEnd = options.metrics.vplayer_success_responses_timing.startTimer()

          log("Loading Playlist: " + playlist)

          const subResponse = await fetch(playlist, { headers })
          if (subResponse.status > 399) throw new Error('Request not successful: ' + subResponse.status)

          timeEnd()
          options.metrics.vplayer_success_responses_total.inc({ playerType: 'hls-live', testName: options.testName })

          const subBody = await subResponse.text()
          if (subBody.includes("X-AD-VAST=")) {
            if (insideAdBreak === false) {
              const url = subBody.split('X-AD-VAST="')[1].split('"')[0]
              try {
                vast.fireBeacons(url, 0, log, options)
              } catch (e) {
                log(e.message)
              }
              log('--> Inside Ad-Break')
              insideAdBreak = true
            }
          } else if (insideAdBreak === true) {
            log('<-- No Longer inside Ad-Break')
            insideAdBreak = false
          }

          const subManifest = hlsParser(subBody)
          if (subManifest.segments.length === 0) throw new Error('No segments in the playlist \nURL: ' + baseUrl + variant + '\n' + subBody)
          const lastSegment = subManifest.segments.length - 1
          const lastUri = subManifest.segments[lastSegment].uri
          sleepDuration = subManifest.segments[lastSegment].duration * 1000
          log("Watching current segment: " + lastUri)
          this.errorsInRow = 0
        } catch (e) {
          errorHandler(e, options, this.errorsInRow, 'hls-live')
          this.errorsInRow++
        }

        await sleep(sleepDuration)
        this.play(manifest, manifestUrl, options, insideAdBreak)
      }
    },
    "dash-live": {
      async init(url) {
        headers.Referer = url;
        const response = await fetch(url, { headers });
        const body = await response.json();
        return body.mediaURL;
      },
      errorsInRow: 0,
      async play(manifest, manifestUrl, options) {
        let sleepDuration = 5000;
        try {
          options.metrics.vplayer_requests_total.inc({ playerType: 'dash-live', testName: options.testName })
          const timeEnd = options.metrics.vplayer_success_responses_timing.startTimer()

          const manifestResponse = await fetch(manifest, { headers });
          if (manifestResponse.status > 399) throw new Error('Request not successful: ' + manifestResponse.status)
          timeEnd()

          const manifestBody = await manifestResponse.text();

          options.metrics.vplayer_success_responses_total.inc({ playerType: 'dash-live', testName: options.testName })

          const parsedManifest = dashParser.parse(manifestBody, manifestUrl);
          const length = parsedManifest.playlists.length

          const variant = parsedManifest.playlists[length - 1].segments[0];

          sleepDuration = variant.duration * 1000;
          log(
            "Playlists: " + length +
            " - Watching current segment: [" +
            new Date().toISOString() +
            "] " +
            variant.uri
          );
          this.errorsInRow = 0;
        } catch (e) {
          errorHandler(e, options, this.errorsInRow, 'dash-live')
          this.errorsInRow++
        }

        await sleep(sleepDuration);
        this.play(manifest, manifestUrl, options);
      }
    }
  }
}

module.exports = players