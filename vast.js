const fetch = require("node-fetch")
const { XMLParser, XMLBuilder, XMLValidator } = require('fast-xml-parser')
const parser = new XMLParser({
  parseAttributeValue: true,
  ignoreAttributes: false,
})
const { promisify } = require("util")
const sleep = promisify(setTimeout)

const convert1000 = (time) => {
  const [h, m, s] = time.split(':')
  let seconds = (h * 3600) + (m * 60) + parseInt(s)
  return seconds
}

async function fireBeacon(url, wait, index, log) {
  return new Promise(async resolve => {
    try {
      if (wait) await sleep(wait)
      log('Sending beacon: ' + url)
      await fetch(url)
      resolve(true)
    } catch (e) {
      resolve(false)
    } finally {
      // --beacons.pending
    }
  })
}

async function fireBeacons(url, index = 0, log) {
  if (process.env.ENABLE_CLIENT_SIDE_TRACKING !== 'true') return

  // download VAST
  const response = await fetch(url)
  const json = await response.json()
  const vastXml = json.vast

  let vastObj = parser.parse(vastXml)

  let ads = vastObj.VAST.Ad
  for (const ad of ads) {
    let duration = 5
    try {
      const proms = []
      for (let impression of ad.InLine.Impression) {
        if (impression['#text']) impression = impression['#text']
        if (impression && impression.length > 1) {
          proms.push(fireBeacon(impression, 0, index, log))
        }
      }
      duration = convert1000(ad.InLine.Creatives.Creative.Linear.Duration)
      for (let event of ad.InLine.Creatives.Creative.Linear.TrackingEvents.Tracking) {
        let url = event['#text'] || event
        if (url && url.length > 1) {
          if (event['@_event'] === 'start') proms.push(fireBeacon(url, 0, index, log))
          if (event['@_event'] === 'firstQuartile') proms.push(fireBeacon(url, duration * 0.25 * 1000, index, log))
          if (event['@_event'] === 'midpoint') proms.push(fireBeacon(url, duration * 0.5 * 1000, index, log))
          if (event['@_event'] === 'thirdQuartile') proms.push(fireBeacon(url, duration * 0.75 * 1000, index, log))
          if (event['@_event'] === 'complete') proms.push(fireBeacon(url, duration * 1000, index, log))
        }
      }
      await Promise.all(proms)
    } catch (e) {
      log(`fireBeacons, ad=${ad['@_id']}, error: ${e.message}`)
    }
    await sleep(duration * 1000)
  }
}

module.exports = {
  fireBeacons
}