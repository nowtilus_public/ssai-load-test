#!/bin/bash
IMAGE=ssai-load-test

PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

TIME=$(date +%s)

REGISTRY_NAME=$(grep REGISTRY_NAME ./.env | xargs)
REGISTRY_NAME=${REGISTRY_NAME#*=}
REGISTRY_USER=$(grep REGISTRY_USER ./.env | xargs)
REGISTRY_USER=${REGISTRY_USER#*=}
REGISTRY_PASSWORD=$(grep REGISTRY_PASSWORD ./.env | xargs)
REGISTRY_PASSWORD=${REGISTRY_PASSWORD#*=}

docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASSWORD} ${REGISTRY_NAME}
docker buildx build -t ${IMAGE}:${PACKAGE_VERSION}-${TIME} --platform linux/amd64 .

docker tag ${IMAGE}:${PACKAGE_VERSION}-${TIME} ${REGISTRY_NAME}/${IMAGE}:${PACKAGE_VERSION}-${TIME}
docker push ${REGISTRY_NAME}/${IMAGE}:${PACKAGE_VERSION}-${TIME}

echo ${REGISTRY_NAME}/${IMAGE}:${PACKAGE_VERSION}-${TIME}