
const Players = require('./players')

const Player = (options, log) => {

  async function startPlayer(options) {
    const player = Players(log)[options.player];
    if (!player || !player.init)
      throw new Error("No such player type implemented");
    try {
      const manifest = await player.init(options.url, options.player);
      log("initialized playback");
      player.play(manifest, options.url, options);
    } catch (e) {
      log("Cannot init playback: " + e.message);
      console.error("Cannot init playback: " + e.message);
      options.exit();
    }
  }

  return { start: startPlayer, options, log }
}

module.exports = Player

