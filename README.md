# Serverside.ai Load-Test

The purpose of this script is to load test the manifest manipulation related components of the Serverside.ai system.

## How to run locally

```bash
# in .env file in the application root dir
URL=https://staging-live.serverside.ai/hls/b95b20c3-ff9d-45c5-9b7b-26bb001659ea/master.m3u8?api-key=4d316ac4-ab31-4ee7-9439-bae421dbd4c9 # Media URL 
PLAYER_TYPE=hls-live # hls-live, dash-live, hls-vod, dash-vod
LOG_LEVEL=info
ENABLE_CLIENT_SIDE_TRACKING=true
CONCURRENT_PLAYERS=2 # For local testing use 1 - for load test up to 1000
PLAYER_START_SLEEP_MS=10 # Time in milliseconds to wait until next virtual player is started 

# then run
node .
```

or use VS-Code and run

## How to deploy

```bash
# in .env file in the application root dir
REGISTRY_PASSWORD=xxxxxxxxxxxxx
REGISTRY_NAME=nowtilusregistry.azurecr.io
REGISTRY_USER=nowtilusregistry

# then run
bash push.sh
```